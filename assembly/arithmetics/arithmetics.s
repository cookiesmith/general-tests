#include <avr/io.h>

.set PINB, 	0x03
.set DDRB, 	0x04
.set PORTB,	0x05

.org 0x00000000
init:
	sbi DDRB, 5
	ldi r16, 0b100
	ldi r17, 42
	ldi r18, 0x11
	add r16, r18	;Deve dar 21
	add r16, r17	;63
	rcall counter
	rcall infinity

infinity:
	rcall infinity

counter:
	count:
		rcall blink
		dec r16
		brne count
ret

blink:
	sbi PORTB, 5
	rcall delay_1s
	cbi PORTB, 5
	rcall delay_1s
ret


delay_1s:
	rcall delay_500ms
	rcall delay_500ms
ret
delay_500ms:
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
ret
delay_100ms:
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
ret
delay_20ms:
	rcall delay_5ms
	rcall delay_5ms
	rcall delay_5ms
	rcall delay_5ms
ret
delay_5ms:
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
ret
delay_1ms:
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
ret
delay_1us:
	push r17
	ldi r17, 99	;Contar 1 microssegundo, usando 100 ciclos.
	delay_1us1:
		nop
		nop
		nop
		nop
		nop
		dec r17
		brne delay_1us1
	pop r17
ret
