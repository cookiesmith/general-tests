#include <msp430g2553.h>

#define COUNT = 50000;

void config_clock(void)
{
	DCOCLK = 
}

config_timer(void)
{
	TA0CTL = TASSEL1 + ID1 + MC0;
	TA0CTL = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT;
	TA0CCR = COUNT - 1;
	TA0CCR1 = COUNT/2;
}

void config_ports(void)
{

}

void config_adc(void)
{
	/* R(ldr) = 2*10^6 ohms;
	/ tsample = (2*10^6 + 2*10^3)*ln(2^11) * 27*10^-12 = 4.1214*10^-4
	/ tsample*f < 64; f < 64/tsample = 1.53*10^5
	/ f < 153kHz, so SMCLK serves well
	/ LDR at A0, so no setting needed
	/
	/
	*/

	ADC10CTL0 = SREF1;
	ADC10CTL0 = ADC10SHT0 + ADC10ON + ADC10IE + MSC;
	ADC10CTL1 = SHS0;
	ADC10AE0 = BIT0;
}


__attribute__((interrupt(ADC10_VECTOR)))

void convert(void)
{
	ADC10CTL0 &= ~ENC;
	reading = ADC10MEM;
	ADC10CTL0 |= ENC;
}

int main(void)
{
	WDCTL = 
	config_clock();
	config_timer();
	config_ports();
	config_adc();
	
	while(1)
	{
	}
}
