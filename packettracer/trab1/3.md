# Topologia 3

### Redes

Network	|IP4		|IP6				|Netmask
LAN0	|172.16.7.0/16	|2001:db8:acad:7::/64		|255.255.0.0
LAN1	|192.168.7.0/24	|2001:db8:acad:8::/64		|255.255.255.0
WAN1	|10.7.1.0/24	|2001:db8:acad:ff01::/64	|255.255.255.252
WAN2	|10.7.2.0/24	|2001:db8:acad:ff02::/64	|255.255.255.252

### Dispositivos

Device	|Interface	|Network	|IPV4		|IPV6			|Netmask	|Gateway		
R1	|FA0/0		|WAN1		|10.7.1.1	|2001:db8:acad:ff01::1	|255.255.255.252|X
	|FA0/1		|LAN0		|172.16.7.254	|2001:db8:acad:7::254	|255.255.0.0	|X		
--------|---------------|---------------|---------------|-----------------------|---------------|--------
R2	|FA0/0		|WAN2		|10.7.1.2	|2001:db8:acad:ff02::2	|255.255.255.252|X	
	|FA0/1		|LAN1		|192.168.7.254	|2001:db8:acad:8::254	|255.255.255.0	|X
--------|---------------|---------------|---------------|-----------------------|---------------|--------
R3	|FA0/0		|WAN2		|10.7.2.1	|2001:db8:acad:ff01::1	|255.255.255.252|X
	|FA0/1		|WAN1		|10.7.1.2	|2001:db8:acad:ff02::2	|255.255.255.252|X		
--------|---------------|---------------|---------------|-----------------------|---------------|--------
S1	|X		|LAN0		|172.16.1.253	|X			|255.255.0.0	|172.16.7.254
S2	|X		|LAN1		|192.168.7.253	|X			|255.255.255.0	|192.168.7.254
PC0.1	|X		|LAN0		|172.16.7.1	|2001:db8:acad:7::1	|255.255.0.0	|172.16.1.254
PC0.2	|X		|LAN0		|172.16.7.2	|2001:db8:acad:7::2	|255.255.0.0	|172.16.1.254
PC1.1	|X		|LAN1		|192.168.7.1	|2001:db8:acad:8::1	|255.255.255.0	|192.168.7.254
PC1.2	|X		|LAN1		|192.168.7.2	|2001:db8:acad:8::2	|255.255.255.0	|192.168.7.254

# Roteador R1

## Routing table

Protocol|Type	|Destiny		|Netmask	|Next Hop		|Exit interface
IPV4	|C	|10.7.1.0		|255.255.255.252|0.0.0.0		|FA0/0
	|S	|10.7.2.0		|255.255.255.252|10.7.1.2		|FA0/0
	|S	|192.168.7.0		|255.255.255.0	|10.7.1.2		|FA0/0
	|C	|172.16.0.0		|255.255.0.0	|0.0.0.0		|FA0/1
--------+-------+-----------------------+---------------+-----------------------+----------
IPV6	|C	|2001:db8:acad:ff01::/64|X		|::			|FA0/0
	|S	|2001:db8:acad:ff02::/64|X		|2001:db8:acad:ff01::2	|FA0/0
	|S	|2001:db8:acad:8::/64	|X		|2001:db8:acad:ff01::2	|FA0/0
	|C	|2001:db8:acad:7::/64	|X		|0.0.0.0		|FA0/1

## Setting
	
	enable
	erase startup-config

	reload
	configure terminal
	hostname R1-AMR

### Interface FA0/0

	interface fa0/0
	description WAN1
	ip address 10.7.1.1 255.255.255.252
	duplex auto
	speed auto
	ipv6 address 2001:db8:acad:ff01::1/64
	ipv6 address fe80::1 link-local
	no shutdown

### Interface FA0/1

	interface fa0/1
	description LAN0
	ip address 172.16.7.254 255.255.0.0
	ipv6 address 2001:db8:acad:7::254/64
	ipv6 address fe80::1 link-local
	no shutdown

### Routing table

	ip route 10.7.1.0 255.255.255.252 0.0.0.0
	ip route 10.7.2.0 255.255.255.252 10.7.1.2
	ip route 192.168.7.0 255.255.255.0 10.7.1.2
	ip route 172.16.0.0 255.255.0.0 0.0.0.0
	ipv6 route 2001:db8:acad:ff01::/64	::
	ipv6 route 2001:db8:acad:ff02::/64 2001:db8:acad:ff01::2
	ipv6 route 2001:db8:acad:8::/64 2001:db8:acad:ff01::2
	ipv6 route 2001:db8:acad:7::/64 0.0.0.0
	ipv6 unicast-routing
	exit
	copy running-config startup-config

# Switch S1

## Setting

	enable
	configure terminal
	interface vlan1
	ip address 172.16.1.253
	no shutdown
	exit
	exit
	copy running-config startup-config

# PC0.1

ipv4 172.16.7.1
netmask 255.255.0.0
gateway 172.16.7.254
ipv6 2001:db8:acad:7::1
gateway 2001:db8:acad:7::254/64

# PC0.2

ipv4 172.16.7.2
netmask 255.255.0.0
gateway 172.16.7.254
ipv6 2001:db8:acad:7::2
gateway 2001:db8:acad:7::254/64

# Roteador R2

## Routing table

Protocol|Type	|Destiny		|Netmask	|Next Hop		|Exit interface
IPV4	|S	|10.7.1.0		|255.255.255.252|10.7.2.1		|FA0/0
	|C	|10.7.2.0		|255.255.255.252|0.0.0.0		|FA0/0
	|C	|192.168.7.0		|255.255.255.0	|0.0.0.0		|FA0/1
	|S	|172.16.0.0		|255.255.0.0	|10.7.2.1		|FA0/0
--------+-------+-----------------------+---------------+-----------------------+----------
IPV6	|S	|2001:db8:acad:ff01::/64|X		|2001:db8:acad:ff02::1	|FA0/0
	|C	|2001:db8:acad:ff02::/64|X		|::			|FA0/0
	|C	|2001:db8:acad:8::/64	|X		|::			|FA0/1
	|S	|2001:db8:acad:7::/64	|X		|2001:db8:acad:ff02::1	|FA0/0

## Setting

	enable
	erase startup-config

	reload
	configure terminal
	hostname R2-AMR

## Interface FA0/0

	interface fa0/0
	description WAN2
	ip address 10.7.2.2 255.255.255.252
	duplex auto
	speed auto
	ipv6 address 2001:db8:acad:ff02::2/64
	ipv6 address fe80::1 link-local
	no shutdown

## Interface FA0/1

	interface fa0/1
	description LAN1
	ip address 192.168.7.254 255.255.255.0
	ipv6 address 2001:db8:acad:8::254/64
	ipv6 address fe80::1 link-local
	no shutdown

### Routing table

	ip route 10.7.1.0 255.255.255.252 10.7.2.1
	ip route 10.7.2.0 255.255.255.252 0.0.0.0
	ip route 192.168.7.0 255.255.255.0 0.0.0.0
	ip route 172.16.0.0 255.255.0.0 10.7.2.1
	ipv6 route 2001:db8:acad:ff01::/64 2001:db8:acad:ff02::1
	ipv6 route 2001:db8:acad:ff02::/64 ::
	ipv6 route 2001:db8:acad:8::/64 ::
	ipv6 route 2001:db8:acad:7::/64 2001:db8:acad:ff02::1
	ipv6 unicast-routing
	exit
	copy running-config startup-config

# Switch S2

	enable
	configure terminal
	interface vlan1
	ip address 192.168.7.253 255.255.255.0
	no shutdown
	exit
	exit
	copy running-config startup-config

# PC1.1

ipv4 192.168.7
netmask 255.255.255.0
gateway 1192.168.7.254
ipv6 2001:db8:acad:8::1
gateway 2001:db8:acad:8::254/64

# PC1.2

ipv4 192.168.7.2
netmask 255.255.255.0
gateway 192.168.7.254
ipv6 2001:db8:acad:8::2
gateway 2001:db8:acad:8::254/64

# Roteador R3

## Routing table

Protocol|Type	|Destiny		|Netmask	|Next Hop		|Exit interface
IPV4	|C	|10.7.1.0		|255.255.255.252|0.0.0.0		|FA0/0
	|C	|10.7.2.0		|255.255.255.252|0.0.0.0		|FA0/0
	|S	|192.168.7.0		|255.255.255.0	|10.7.2.2		|FA0/0
	|S	|172.16.0.0		|255.255.0.0	|10.7.1.1		|FA0/1
--------+-------+-----------------------+---------------+-----------------------+----------
IPV6	|C	|2001:db8:acad:ff01::/64|X		|::			|FA0/0
	|C	|2001:db8:acad:ff02::/64|X		|::			|FA0/0
	|S	|2001:db8:acad:8::/64	|X		|2001:db8:acad:ff02::2	|FA0/0
	|S	|2001:db8:acad:7::/64	|X		|2001:db8:acad:ff01::1	|FA0/1

## Setting

	enable
	erase startup-config

	reload
	configure terminal
	hostname R3-AMR

### Interface FA0/0

	interface fa0/0
	description WAN2
	ip address 10.7.2.1 255.255.255.252
	duplex auto
	speed auto
	ipv6 address 2001:db8:acad:ff02::1/64
	ipv6 address fe80::1 link-local
	no shutdown

### Interface FA0/1

	interface fa0/1
	description WAN1
	ip address 10.7.1.2 255.255.255.252
	duplex auto
	speed auto
	ipv6 address 2001:db8:acad:ff01::2/64
	ipv6 address fe80::1 link-local
	no shutdown

### Routing table

	ip route 10.7.1.0 255.255.255.252 0.0.0.0
	ip route 10.7.2.0 255.255.255.252 0.0.0.0
	ip route 192.168.7.0 255.255.255.0 10.7.2.2
	ip route 172.16.0.0 255.255.0.0 10.7.1.1
	ipv6 route 2001:db8:acad:ff01::/64 ::
	ipv6 route 2001:db8:acad:ff02::/64 ::
	ipv6 route 2001:db8:acad:8::/64 2001:db8:acad:ff02::2
	ipv6 route 2001:db8:acad:7::/64 2001:db8:acad:ff01::1
	ipv6 unicast-routing
	exit
	copy running-config startup-config


