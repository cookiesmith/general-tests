/* Faça um programa em 2 processos são iniciados utilizando fork cada um
 * com as seguintes ações:
 *
 *	Os filho 1 irá executar cálculo 2*3, e retornar ao pai, via exit()
 *	o resultado da equação (utilize exec() para fazer isso)
 *
 *	O filho 2 irá entrar em looping infinito
 *
 *	O pai, após receber o resultado do filho 1, irá terminar o looping
 *	infinito do filho 2. Para isso o pai deve enviar um sinal ao filho
 *	2, esse deve capturar e mudar o valor de iteração do while() ou
 *	for())
 *
 *	O filho 2, agora acordado, deve imprimir o resultado
 *
 *	Ambos os filhos devem virar zoobies
 */

int main(void)
{
	return (0);
}
