ibrary ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity example_test is
end example_test;
 
architecture tset_elpmaxe of example_test is
 
	constant period : time := 10 ns;
 
	signal test_a : std_logic;
	signal test_b : std_logic;
	signal test_c : std_logic;
	signal test_d : std_logic;
 
	component variable_vs_signal is
		port (
			i_clk		: in std_logic;
			o_var_done	: out std_logic;
			o_sig_done	: out std_logic
			);
	end component variable_vs_signal;
	 
	begin
		clock <= not clock after period/2;
		variable_vs_signal_inst : variable_vs_signal port map (
			i_clk => r_Clock,
			o_var_done => w_Var_Done,
			o_sig_done => w_Sig_Done
		);
		 
end behave;
