
# Vpp/2 = Vff(1+R1/R2);
#
# VS = Vpp/(2*(1+R1/R2));
#
# With R1 = R2 = 10Kohms, Vff = Vpp/4;

#TODO: Assure the circuit never saturates
1;

function [R1, R2, VFF] = offset(VPP, R1)
	global VCC;
	R1 = R1;
	R2 = R1;
	VFF = VPP/4;
endfunction
