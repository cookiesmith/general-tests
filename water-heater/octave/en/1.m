% Burocracy (bleh)
clc;
clear;
close all;

% Package loading
pkg load io;
pkg load signal;

% Include the functions for each chart
source charts.m

## DOCUMENTATION:

## NAME:
#
#	PTC - Project of Temperature Control

## AUTHOR:
#
#	Allan Ribeiro

## VERSION:
#
#	v1.2

## LICENSE:
#
#	Copyright © 2022 Free Software Foundation, Inc.  License GPLv3+:
#	GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
#	This is free software: you are free to change and redistribute it.
#	There is NO WARRANTY, to the extent permitted by law.

## DESCRIPTION:
#
#	This script was created as course requirement for Integration
#	Workshop, douring Computer Engeneering, Bachelor degree at UTFPR.
#
#	The goal is to control a thermic system, which consists in a water
#	heater, and it will be measured constantly and controled to a fixed
#	at a given temperature. The script serves as comparsion between
#	theorical and practical modelings.

## THEORICAL MODEL:
#
#	```
#
#	FIGURE 1 - System representation
#
#			Ext. env.
#			.................
#			.Water		.
#		+-------+-------+	.
#		|	.	|	.
#		Vcc	.	R-> Hi	. -> Ho
#		|	.	|	.
#		+-------+-------+	.
#			.		.
#			.................
#
#	The inserted energy driven by the voltage over the resistor, and
#	the loss energy, as the system is not isolated.
#
#	```
#
#	The system may be conceived as a resistor, immersed in water, with
#	voltage controlled by a voltage source. The water have it's
#	temperature sensored to keep control of the system state.
#
#	The function is defined as the product between Thermic resistance
#	and Thermic power inserted into the system:
#
#		Qi - Qo = m*c*(T-To)
#
#	Because the system runs trough time, we take it's variation
#
#		(Qi - Qo)/dt = m*c*(dT/dt)
#
#	As "Q/dt" is the rate of energy inserted into system, it may be
#	rewritten:
#
#		Hi - Ho = m*c*(dT/dt)
#
#	Defining
#
#		Rt = T/Ho; (eq.1)
#
#	as a constant, known as thermic resistance of the system, and
#
#		C = m*c; (eq.2)
#
#	as the thermic capacitance:
#
#		Hi - T/Rt = C*(dT/dt)
#
#		Hi = C*(dT/dt) + T/Rt
#
#		Hi*Rt = (Rt*C)*(dT/dt) + T; (eq.3)
#
#	With the Laplace transform, it is defined as:
#
#		G(s) = T(s)/Hi(s) = Rt/(Rt*C*s + 1); (eq.4)
#
#	```
#
#	FIGURE 2 : Transfer function
#
#			+-------+
#			|	|
#		Hi(s)->	|  G(s)	| -> T(s)
#			|	|
#			+-------+
#
#	The system is fed with entry power, and varies in temperature.
#
#	```
#
#	With Hi as thermic power inserted into system, in Kcal, T as
#	temperature function (system behaviour), in ºC, C as thermal
#	capacitance, C = m * c, with m as water mass [Kg], c as specific
#	heat capacity [Kcal*ºC/Kg], and Rt as thermic resistance.

% System's thermal capacitance

m = 0.51; % Water mass
c = 1; % Specific heat capacity, in joules
global C = m*c % Thermic capacitance

## ELECTRICAL RESISTANCE
#
#	According to electrical definition of power, electric power is the
#	rate, in time, of electrical energy tranfered by an electric
#	circuit. It may be written as:
#
#	P = W/t, where P[W] is the power, W[J] is the tranfered energy, and
#	t[s] is the needed time.
#
#	By inserting the definition of current in the equation, we may
#	write:
#
#	P = (W/Q) * (Q/t) = V*I;
#
#	Finnaly, by the use of Ohm's law (V = R*I), we rewrite the equation as:
#
#	P = V^2/R;
#
#	And manipulating, we are allowed to find the theorical resistance
#	of the heater
#
#	R = V^2/P; (eq.4)
#
#	For this case, we'll be considering RMS voltage (127V)

% Heater data: Heater RQ 127V, 770W
heater_voltage = 127;
heater_power = 770;
global heater_electric_resistance = heater_voltage ^ 2 / heater_power

## THERMAL RESISTANCE
#
#	According to Thermal Resistance equation (Fourier's Law for one
#	dimension), we have a heat flux through a suface (normal area),
#	given by a temperature gradient, as it follows:
#
#		q = -k*dT/dx
#
#	With q[W/m^2] as heat flux, k[W/(K*m)] as the thermal conductivity of the
#	material, and dT/dx[K/m] as the variation of temperature per distance of
#	the material. When integrating in a variation of time:
#
#		Q = -k*A*dT/dx
#
#	Where Q[W] is the flux. Considering the used material, we have the
#	area of conductivity not as a plane, but as a cilinder, which we
#	equationate as it follows:
#
#		Rt = L/(k*A)
#
#	With: L[m] being the thickness of the material, k[W/(K*m)] as the
#	thermal conductivity of the sample, and A[m^2] is the cross-sectional
#	area. But taking the temperature gradient, and heat flux, across the same
#	sample, we get
#
#		Rt = L*T/(A*Hi*L) = T/Hi;
#
#	Which reiterate eq.1.
#

## GAIN AND TIME CONSTANTS
#
#	In the form of a type 0 linear system:
#
#		Rt / (s*(Rt*C) + 1) = Kcc / (s*tau + 1)
#
#	Where: "Kcc" is a constant that defines the output variation
#	according to the input variation, Rt;
#	"tau" is another constant, which defines the time that the system
#	needs to set: Rt*C.
#
#	This values were determined by observation: tau is the time
#	needed for the system to reach 63.21 % of it's final value, and Kcc as the
#	ratio between the system entry of energy by the system loss of energy when
#	the system sets.

function retval = avg(v) % Average of n numbers
	retval = sum(v)/length(v);
endfunction

function retval = time_constant(v) % Time constant retrieval based in given system
	n = length(v);
	value = avg(v(n-20:n)); % Get average value in last 20 points, to serve as setting value
	value = ceil(value*63.5/100); % Extract 63.5% of the value
	j = 1;
	while(v(j) < value)
		j++;
	endwhile
	retval = j % Time constant extraction
endfunction

#
#	The practical system entry is actually voltage, a conversion
#	block is added to the system:
#
#		P = V^2/R;
#
#	Which is given in joules. One Joule is Kph = 2.39*10^-4 Kcal, and so:
#
#		Hi = Kph*V^2/R;
#
#	```
#
#	FIGURE 3 : Transfer function, taking voltage as entry
#
#				+-------+		+-------+		+-------+
#				|	|		|	|		|	|
#		 -> V(s) ->	| V^2/R	|  -> P(s) ->	|  Kph	|  -> Hi(s) ->	| G(s)	|  -> T(s)
#				|	|		|	|		|	|
#				+-------+		+-------+		+-------+
#
#	The system varies in tension, which will cause a power variation,
#	thus, changing the system temperature;
#
#	```
#
#	AS Kcc is, actually, the thermal resistance of the system,
#
#	kcc = Rt = T/Hi; (eq.5)
#
#	tau = t, if g(t) = g(t->infinity)*0.63; (eq.6)
#

% Gain constant retrieval based in given system

% System step value (in V)
global sv = 37;

% Constant of conversion from joules to cal
global Kph = 2.39*10^-4;

% Export of internal functions which must become global

global Hi = Kph*sv^2/heater_electric_resistance;

function retval = gain_constant(v)
	global heater_electric_resistance;
	global Kph;
	global sv;

	global T = v(length(v))-v(1);
	global Hi = Kph*sv^2/heater_electric_resistance;
	retval = T/Hi;
endfunction

% First system block

## MODEL VALIDATION
#
#	The model validation consists in affering wheter your theorical
#	model is a valid aproximation for a practical system. Thus,
#	plotting both theorical and practical data in overlay is a
#	efficient way.
#

% Experimental model

function retval = experiment
	% Experimental data importing
	chart = csv2cell("experiment.csv");

	% Number of points in the experiment
	n = size(chart)(1);

	% Optimization stuff
	aux1 = 1:n; % Time intervals
	aux2 = ones(n,1); % Vector full of number one for faster operation

	% Extraction of the value of each point
	b = cell2mat(chart(aux1, 3));

	% Offset adding
	retval = b - aux2*b(1);
endfunction

% Theorical model, apliyng practical contants

global g1;
global g1n;
global g1d;

function retval = theory1 (tau, kcc, n)
	global Hi;
	global g1;
	global g1n;
	global g1d;

	% Space state specification
	A = [-1/tau];
	B = [kcc/tau];
	C = 1;
	D = 0;

	% Construction of numerator and denominator of Transf. Func.
	[g1n, g1d] = ss2tf(A, B, C, D);

	% Construction of Transf. Func.
	g1 = tf(g1n, g1d);

	g1 = g1*Hi;

	y = step(g1, 1:n);

	retval = y;
endfunction

% Theorical model, apliyng theorical constants

global g2;
global g2n;
global g2d;

function retval = theory2 (c1, c2, n)
	global Hi;
	global g2;
	global g2n;
	global g2d;

	% Construction of numerator and denominator of Transf. Func.
	g2n = c1;
	g2d = [(c1*c2), 1];

	g2 = tf(g2n, g2d);

	g2 = g2*Hi;

	y = step(g2, 1:n);

	retval = y;
endfunction

% If you want to regenerate the charts, set CHARTS to 1
GENCHARTS = 1;

### NOISE IN MEASURE METHOD

#	As neither the measures, and the method used for measuring are
#	ideals, the function aquired is full of "spikes", which is
#	undesired. One method of smooth the function noise is to exchange
#	each time sample by it's average between n samples ahead, as
#	follows:
#	
#	average-function(t) = sum(f(t))(from t to t+n)/n, where n is the
#	number of dots used in the average.
#
#	Chart 5 shows the comparsion between original (red) function and the
#	aproximated (green) function
#
#	<details>
#	<summary><b>Chart 5</b></summary>
#
#	![](5.png)
#
#	</details>
#
#	Note: unless specified otherwise, the aproximated function will be
#	used.
#

% Aproximate a function to the average of n samples ahead
function retval = unoise(v, n)
	nv = length(v);
	if n < nv % the number of dots used in the average
		limit = nv-n;
		for j = 1:limit
			retval(j) = avg(v(j:j+n));
		endfor
		% TODO: Avegare between the last points, when possible
		for k = j+1:nv
			retval(j) = v(k);
		endfor
	endif
endfunction

% If you want the experiment function to be aproximated, set USEAVERAGE to the
% desired number of dots to be used in the average. USEAVERAGE need to be
% less than the number of timedots;

global USEAVERAGE = 0;

% If you want to see the comparsion between original and the averages, set COMPAREAVERAGE to 1
COMPAREAVERAGE = 1;

if USEAVERAGE > 0
	exp2_data = experiment();
	global exp_data = unoise(exp2_data, USEAVERAGE);
	global n = length(exp_data);
	if COMPAREAVERAGE
		if GENCHARTS
			chart5(exp2_data, exp_data, n);
		endif
	endif
else
	global exp_data = experiment();
	global n = length(exp_data);
endif

global tau = time_constant(exp_data);
global kcc = gain_constant(exp_data);
global heater_thermic_resistance = kcc;

% If you want to validate models, which is to compare theoricals AND real
% model, set VALIDATION to 1, for charts superposition, or EXPERIMENTAL and
% THEORICAL, for independent charts for each function. Setting EXPERIMENTAL only will plot
% EXPERIMENTAL chart, and THEORICAL only, or set none, the THEORICAL chart
% will be plotted.

VALIDATION = 1;
EXPERIMENTAL = 1;
THEORICAL = 1;

global g1_data = theory1(tau, kcc, n);
global g2_data = theory2(kcc, C, n);
% Plot experimental and theorical graphs together
if VALIDATION
	if GENCHARTS
		chart1(exp_data, g1_data, g2_data, n);
	endif
elseif EXPERIMENTAL & THEORICAL % Plot both models in different charts
	if GENCHARTS
		chart2(exp_data, n);
		chart3(g1_data, n);
		chart4(g2_data, n);
	endif
elseif EXPERIMENTAL % Plot only experimental model
	if GENCHARTS
		chart2(exp_data, n);
	endif
else % Plot only theorical model
	if GENCHARTS
		chart3(g1_data, n);
		chart4(g2_data, n);
	endif
endif

## ELECTRICAL EQUIVALENT MODEL

#

## DATA ANALISYS AND CHOOSE OF MODEL

#	The chart 1.png shows the system response for the real system, in
#	red, the theorical response, using the practical constans, in
#	green, and the theorical response, using theorical constants, in
#	blue. For better view, the same functions are plot, sepratedly, in
#	charts 2 (experiment only), 3 (theorical, with practical constants),
#	and 4 (theorical, with theorical constants).
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](1.png)
#	</details>
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](2.png)
#	</details>
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](3.png)
#	</details>
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](4.png)
#	</details>
#
#	The most close curve to the practical system is not obvious,
#	at first glance, and for this ocasion, we'll calculate the
#	difference for each point in the system:
#
#	delta(t) = |exp_data(t) - g1_data(t)|
#
#	The results may be seen in chart 6
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](6.png)
#	</details>
#
#	And, yet, the result is not clear to our eyes. We have one more
#	resouce, which is to calculate the total error accumulated in time: Integrating the error for every simulated point
#
#	integral(t) = delta(t), for t = 0
#
#	integral(t) = integral(t-1) + delta(t); t > 1
#
#	The results may be seen in the chart 7.
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](7.png)
#	</details>
#

delta1(1) = abs(exp_data(1)-g1_data(1));
integ1(1) = delta1(1);
%intteg1(1) = integ1(1); %Integral of integral
delta2(1) = abs(exp_data(1)-g2_data(1));
integ2(1) = delta2(1);
%intteg2(1) = integ2(1); % Integral of integral
for j = 2:n
	delta1(j) = abs(exp_data(j)-g1_data(j));
	integ1(j) = integ1(j-1)+delta1(j);
	%intteg1(j) = intteg1(j-1)+integ1(j);
	delta2(j) = abs(exp_data(j)-g2_data(j));
	integ2(j) = integ2(j-1)+delta2(j);
	%intteg2(j) = intteg2(j-1)+integ2(j);
endfor

if GENCHARTS
	chart6(delta1, delta2, n);
	chart7(integ1, integ2, n);
	%chart7(intteg1, intteg2, n); % see integral of integral
endif

#
#	In chart 6 we see that, for aprox. 1800s, G2 has a bigger
#	difference from the experiment, almost 4 degrees, and consequently it's integral (chart 7)
#	raises faster; Though, G2 difference decreses arround 2700s,
#	getting as small as 0, having a second peak at 4200, with 1 degree
#	of difference. The integral as well turns to grow back
#	,but in smaller tax.
#
#	G1 stays closer to experiment until 1300s, in aprox. 1 degree, but separate quickly
#	until 3000s, and turns to bounce arround 3 degrees, setting majorly
#	arround this point.
#
#	In chart 7 we see that the G2 function difference grows
#	faster for t < 1600, the final integral value is bigger for G1, but
#	for the major part of the time G2 has a lower error growth tax than G1.
#
#	After analysing the charts, the ideal control system is G2, as the needed energy for correction
#	will be lower for the most time, in which the error sum stops
#	growing (after 2500s), and has the smaller error for every time
#	after 1800s.
#

SYSTEM=2;

if SYSTEM == 1
	num = g1n;
	den = g1d;
else
	num = g2n;
	den = g2d;
endif

g = tf(num*Hi, den);

# ACTUATOR EFFECTS IN CONTROL LOOP

#	There are several ways to control the system activity, whether
#	using MOSFETS, IGBT, direct sources, and other components. The IGBT
#	is a low voltage controled switch, providing fast switch when
#	polarized in the specified voltage.
#
#	As being a switch, it is possible to use as a PWM (Pulse Width
#	Modulation) source, in which when it's aplied a DC, the device
#	stablish a direct
#	conversion between the high voltage sourced from the original
#	voltage aplied in the heater (127V), and a reliable low voltage for
#	control (5V). As the grid signal is a AC signal, we're not aplying
#	a direct voltage to the heater, but aplying cycles of current
#	instead. The average voltage this system generates is
#	given as it follows:
#
#	```
#
#	FIGURE N1
#		Grid voltage
#		   V
#		   ^
#	       VRMS|     _	 _
#		   |    / \     / \
#		   |   [   ]   [   ]
#		   |\_/     \_/     \
#		   +---.---.--------> t(s)
#		   0   TX  T
#		Conrol Voltage
#		   V
#		   ^
#		VCC|   +---+   +---+
#		   |   |   |   |   |
#		   |   |   |   |   |
#		   |   |   |   |   |
#		   |___|   |___|   |
#		   +---.---.--------> t(s)
#		   0   TX  T
#
#		Output voltage
#		   V
#		   ^
#	       VRMS|     _	 _
#		   |    / \     / \
#		   |   [   ]   [   ]
#		   |   |   |   |   |
#		   +---.---.--------> t(s)
#		   0   TX  T
#
#	The PWM is a switchng between VCC and 0, in a given duty cycle
#	(ratio of vcc timing by 0v timing). VRMS is only aplied to the
#	circuit when the control voltage is other value but zero.
#
#	```
#
#	Toff = Tx
#	Ton = T-Tx
#
#	D = Ton/T;	
#
#	Defining integral as integral(V(t), To, Tf) = intregral of V(t), in
#	the interval from To (lower limit) to Tf (upper limit):
#
#	Vavg = 1/T * integral(V(t), 0, T)
#	= 1/T * (integral(Vcc, 0, Ton) + integral(0, Ton, T))
#	= Vcc * Ton/T
#	= Vcc * D; (eq. N2)
#
#	The grid voltage is up to 127V(RMS), but we may simplify as a DC
#	source with 180V, as the following equation is true:
#
#	(Vrms)^2 = Ton * (Vp)^2 / (2 * T);
#
#	For Vrms = 127V, at maximum PWM, Vp = 180V;
#
#	Note that, at maximum PWM, Ton = T;
#
#	For PWM operation at 37V as peak, we may calculate the necessaire
#	gain for the circuit:
#
#	Kpwm = 127^2/37 = 435.92;
#
#	Another speficiation is the form of the voltage funciton used in
#	the PWM, which does not 
#
#	D = Vpwmin/Vtripico
#
#	```
#
# 	FIGURE N1+1
#
#	The relation between PWM cycles, saw-tooth function, and control
#	voltage
#
#	```
#
#	```
#
#	FIGURE N1+2 : Transfer function, taking PWM voltage as entry
#
#			+-------+		+-------+		+-------+		+-------+
#			|	|		|	|		|	|		|	|
#		Vpwm -> | Kpwm	|  -> V(s) ->	|  V/R	|  -> P(s) ->	|  Kph	|  -> Hi(s) ->	| G(s)	|  -> T(s)
#			|	|		|	|		|	|		|	|
#			+-------+		+-------+		+-------+		+-------+
#
#	The system varies in tension, which will cause a power variation,
#	thus, changing the system temperature;
#
#	```
#

% Grid info

RMSv = 127;
RMSf = 60;

# ACTIVATION METHOD
#
## SOLID STATE RELAY
#
#	The selected method of handling the current through the circuit was
#	using a Solid State Relay, which works based in a PWM signal,
#	permiting the flow of current in it's output, is this case, the
#	power plug, with 127V as output. The practical voltage the heater
#	sees will be:
#
#	Vef = Vp*sqrt(Ton/(2*T))
#
#	The relation between PWM cycles and the RMS voltage is not linear,
#	though> it's squared. The relation changes, then, to:
#
#	Vef^2 = Vp^2 * Ton/(2*T)
#
#	In practical terms, the grid voltage is 127Vrms, at 60Hz of
#	frequency, resulting in 180V DC. As the relay does not support
#	60Hz, the value will have only 6Hz, a peak of 10V, resulting in:
#
#	f = 6, thus T = 1/6
#
#	Vef = 180*sqrt(1/6/(2*(1/6))) = 127.28 Vrms
#
#	The total gain in this device is:
#
#	Kr = vout/vin = 127.28^2/10 = 1620.02
#

## PWM generation
#
#	For the PWM generation, we make use of a Schmitt trigger, to
#	generate a well-defined square function, a integrator, to transform
#	it in a saw-tooth function, and a comparator, which will decide
#	whether the voltage must be, or not, aplied in the circuit.
#	It's output is a square function, defined in function of a
#	duty cycle, as defined for the control input.
#	
#	```
#
#	FIGURE N1+2
#
#	The Schmitt trigger, coupled with a integrator circuit: the output
#	is a saw-tooth function
#
#	```
#
#	As parameter of activation, an observation must be made: The
#	PWM depends upon a certain signal frequency to work. If
#	the used frequency for the pwm is aproximatelly equal to the grid,
#	or higher, the circuit logic operation does not occurs in valid
#	time, thus, generating wrong signal answer. The desired value is
#	that PWM has 10x less frequency than the grid: 6Hz 
#

% PWM info

PWMin = 10; % Desired PWM output voltage
PWMt = 10/(RMSf); % PWM period
PWMd = 0.5; % Duty cycle
PWMton = PWMd*PWMt; % Period in wich PWM is on high
Vef = 127*sqrt(PWMton/(2*PWMt)); % PWM effective voltage
%Kr = Vef^2/PWMin;
Kr = 127^2/PWMin;

#	The circuit equations are as it follows:

# SHCMITT TRIGGER (NON-INVERSOR)

#
#	The Schmitt trigger output tension:
#
#	Vout = Vsat;
#
#	Setting R1 and R2 of the Schmitt circuit:

SCHMITTin = 10;
SCHMITTr1 = 10*10^3;
SCHMITTr2 = 20*10^3;
SCHMITTout = 13.5;

# INTEGRATOR
#
#	The integrator will recieve Schmitt output tension, and sum for
#	each time stamp, thus generating the integral of the schmitt, which
#	consists in a triangular signal.
#
#	The output frequency of the integrator is
#
#	f = R2/(4*R1*R3*C);
#
#	Setting Vout = 10Vpp and f = 6Hz,
#
#	R2 = 2*R1*Vsat/(Vout);
#
#	f = 1/(2*pi*R2*C) [pertence, p82]

INTEGRATORf = 1; %[pertence, p82]
INTEGRATORc = 10*10^-6;
INTEGRATORr2 = 1/(2*pi*INTEGRATORf*INTEGRATORc);
#INTEGRATORr1 = 10/(INTEGRATORf*INTEGRATORc);
INTEGRATORr1 = 200*10^3;
INTEGRATORr3 = INTEGRATORr1*INTEGRATORr2/(INTEGRATORr1+INTEGRATORr2);
INTEGRATORfl = 1/(2*pi*INTEGRATORr2*INTEGRATORc); % Above this frequency, is an integrator, below, a inversor

# OFFSET

#
#
#
#
#
#
#	With the output being linear in function of V^2, we rewrite the
#	current system:
#
#	FIGURE N1+3 : Transfer function, taking PWM voltage as entry
#
#			+-------+		+-------+		+-------+		+-------+
#			|	|		|	|		|	|		|	|
#		Vpwm -> |  Kr	|  -> V^2(s) ->	|  1/R	|  -> P(s) ->	|  Kph	|  -> Hi(s) ->	| G(s)	|  -> T(s)
#			|	|		|	|		|	|		|	|
#			+-------+		+-------+		+-------+		+-------+
#
#	The system varies in tension, which will cause a power variation,
#	thus, changing the system temperature;
#
#	```
#
#	As the triangular function is not set with 0 as minimum value, a
#	offset is needed, in 5V, so the inferior value of the function is
#	0. 
#

Vout = 10; % See project annotations
Vsat = 13.5; % see datasheet
R1 = 10*10^3; % See project annotations
R2 = 2*R1*Vsat/Vout;
f = 6; % See project annotations
R3 = R2/(4*R1*f*C);



Ks = 0.01;

num = num*Ks*Kr*(1/heater_electric_resistance)*Kph;
G = tf(num, den);
GMF = feedback(G,1);

# INTEGRAL CONTROL

ts = 1200;
mp = 0.1;

aux = log(mp)^2;

sigma2 = aux/(aux+pi^2);
sigma = sqrt(sigma2);

omegan = 4/(ts*sigma);

s(1) = -sigma*omegan + i*omegan*sqrt(1-sigma2);
s(2) = -sigma*omegan - i*omegan*sqrt(1-sigma2);

sumnum = 0;
for term = length(num):-1:1
	sumnum = sumnum + num(length(num)-term+1)*s(1)^(term-1);
endfor

sumden = 0;
for term = length(den):-1:1
	sumden = sumden + den(length(den)-term+1)*s(1)^(term-1);
endfor

abs(sumnum/sumden)

iz = 0.001;
Ti = 1/iz

sumnum = sumnum * (s(1)+iz);
sumden = sumden * s(1);

Kp = 1/abs(sumnum/sumden)
Kp*abs(sumnum/sumden)

gcnum = [Kp, Kp*iz];
gcden = [1 0];
abs(gcnum/gcden)

GC = tf(gcnum, gcden)

GCMF = feedback(GC*G, 1);

control = step(GCMF, 1:10000);
g_data = step(G, 1:10000);

plot(1:10000, g_data(1:10000), "b;system;")
hold
plot(1:10000, control(1:10000), "r;Controled;")

C2 = 100*10^-6
R2 = Ti/C2
R1 = 100*10^3
R3 = 100*10^3


R4 = Kp * R1 * R3 / R2




%{
## HEATER SUPERFICIAL AREA

#	The heater consists in a resistance which circles a plastic
#	structure. It have small loops, enhancincg contact area (similar to
#	shower resistances). The first "loop" refers to those small loops.
#	The loop's loop refers to the circle made around plastic structure
#
#	Values sufixed by * are measured by the use of a caliper (+-0.5mm
#	of precision)
#
# 	Radius (r)*
#
#	Wire circunference (wc): wc = 2*pi*r;
#
#	Loop Radius (lr)*
#
#	Loop circunference (lc):
#
#		lc = 2*pi*lr;
#
#	Area of one loop (al):
#
#		al = wc*lc;
#
#	Distance betwenn two loops (dl)*
#
#	Radius of one loop's loop (lld)*
#
#	Loop's loop circunference (llc):
#
#		llc = 2*pi*llr;
#
#	Area of one loop's loop (alll):
#
#		alll = llc*al;
#
#	Number of loop's loop (llln)*
#
#	Remaining not looped wire (nlw)*
#
#	Total area:
#
#		a = alll*lln + nlw;

## AIR CONTACT AREA
# 	Cup Radius(cr):
#
#	Air contact area
#
#		aca = pi*cr^2;

## STEEL CONTACT AREA
#
#	given by the volume of water divided by area of cup's base, which
#	gives the height:
#
#		h = wm/aca;
#
#	And we must mutiply by the cup circunference, plus the area of the
#	bottom:
#
#		sca = 2*pi*cr*h + aca;
#

% Steel themal conductivity (stc)[W/(mK)]

% Air thermal conductivity (atc)[W/(mK)]

%	precision: +-0.5mm

% TODO: Document this whole lot of accountings

% Radius
r = 0.125;
% Wire circunference
wc = 2*pi*r;
% Loop Radius
lr = 1;
% Loop circunference
lc = 2*pi*lr;
% Area of one loop
al = wc*lc;
% Distance betwenn two loops
dl = 1.25;
% Radius of one loop's loop
llr = 7;
% Loop's loop circunference
llc = 2*pi*llr;
% Area of one loop's loop
alll = llc*al;
% Number of loop's loop: llln*
lln = 6;
% Remaining not looped wire:
nlw = 20;
% Total area:
a = alll*lln + nlw;

% AIR CONTACT AREA
% Cup Radius
cr = 59;
% Air contact area
aca = pi*cr^2;

% Cup height
%h = wm/aca;
% stell contact area
%sca = 2*pi*cr*h + aca;

% Steel themal conductivity
stc = 52;

% Air thermal conductivity
atc = 0.023;
%}


% Source:TDGC2-1.8KVA
% Sensor: ICEL MD-6510


# CONTROL MODELING




## REFERENCES
#
#	[] citations/ogata.bibtex
#
#	[2 - Manual](http://www.icel-manaus.com.br/download/MD-6150%20Manual.pdf), last access: 04-28-2022 [MM-DD-YYYY]
#
#	[3] https://carlroth.com/medias/SDB-1980-PT-PT.pdf?context=bWFzdGVyfHNlY3VyaXR5RGF0YXNoZWV0c3wzMzg1MzZ8YXBwbGljYXRpb24vcGRmfHNlY3VyaXR5RGF0YXNoZWV0cy9oMzgvaGJjLzg5NzExMjAxNDg1MTAucGRmfDM3NGQzNDdhNzZkNjUwY2I3OThjODExNDZmZTJkODIwMTNiNWNlMjIyNTkxZTNkYzBlMmY5MDY4Mjc5NjA4ZDc

## THE MEASURING

#	For the measuring of water heating process, the following was used:
#		* MD-6510 multimeter [1], Heater(770W);
