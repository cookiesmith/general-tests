
# DOCUMENTATION:

# NAME:

PTC - Project of Temperature Control

# AUTHOR:

Allan Ribeiro

# VERSION:

v1.2

# LICENSE:

Copyright © 2022 Free Software Foundation, Inc.  License GPLv3+:
GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

# DESCRIPTION:

This script was created as course requirement for Control Systems
2, douring Computer Engeneering, Bachelor degree at UTFPR.

The goal is to control a thermic system, which consists in a water
heater, and it will be measured constantly and controled to a fixed
at a given temperature. The script serves as comparsion between
theorical and practical modelings.

# THEORICAL MODEL:

```

FIGURE 1 - System representation

		Ext. env.
		.................
		.Water		.
	+-------+-------+	.
	|	.	|	.
	Vcc	.	R-> Hi	. -> Ho
	|	.	|	.
	+-------+-------+	.
		.		.
		.................

The inserted energy driven by the voltage over the resistor, and
the loss energy, as the system is not isolated.

```

The system may be conceived as a resistor, immersed in water, with
voltage controlled by a voltage source. The water have it's
temperature sensored to keep control of the system state.

The function is defined as the product between Thermic resistance
and Thermic power inserted into the system:

	Qi - Qo = m * c * (T-To)

Because the system runs trough time, we take it's variation

	(Qi - Qo)/dt = m * c * (dT/dt)

As "Q/dt" is the rate of energy inserted into system, it may be
rewritten:

	Hi - Ho = m * c * (dT/dt)

Defining

	Rt = T/Ho; (eq.1)

as a constant, known as thermic resistance of the system, and

	C = m * c; (eq.2)

as the thermic capacitance:

	Hi - T/Rt = C * (dT/dt)

	Hi = C * (dT/dt) + T/Rt

	Hi * Rt = (Rt * C) * (dT/dt) + T; (eq.3)

With the Laplace transform, it is defined as:

	G(s) = T(s)/Hi(s) = Rt/(Rt * C * s + 1); (eq.4)

```

FIGURE 2 : Transfer function

		+-------+
		|	|
	Hi(s)->	|  G(s)	| -> T(s)
		|	|
		+-------+

The system is fed with entry power, and varies in temperature.

```

With Hi as thermic power inserted into system, in Kcal, T as
temperature function (system behaviour), in ºC, C as thermal
capacitance, C = m  *  c, with m as water mass [Kg], c as specific
heat capacity [Kcal * ºC/Kg], and Rt as thermic resistance.

# ELECTRICAL RESISTANCE

According to electrical definition of power, electric power is the
rate, in time, of electrical energy tranfered by an electric
circuit. It may be written as:

P = W/t, where P[W] is the power, W[J] is the tranfered energy, and
t[s] is the needed time.

By inserting the definition of current in the equation, we may
write:

P = (W/Q)  *  (Q/t) = V * I;

Finnaly, by the use of Ohm's law (V = R * I), we rewrite the equation as:

P = V^2/R;

And manipulating, we are allowed to find the theorical resistance
of the heater

R = V^2/P; (eq.4)

For this case, we'll be considering RMS voltage (127V)

# THERMAL RESISTANCE

According to Thermal Resistance equation (Fourier's Law for one
dimension), we have a heat flux through a suface (normal area),
given by a temperature gradient, as it follows:

	q = -k * dT/dx

With q[W/m^2] as heat flux, k[W/(K * m)] as the thermal conductivity of the
material, and dT/dx[K/m] as the variation of temperature per distance of
the material. When integrating in a variation of time:

	Q = -k * A * dT/dx

Where Q[W] is the flux. Considering the used material, we have the
area of conductivity not as a plane, but as a cilinder, which we
equationate as it follows:

	Rt = L/(k * A)

With: L[m] being the thickness of the material, k[W/(K * m)] as the
thermal conductivity of the sample, and A[m^2] is the cross-sectional
area. But taking the temperature gradient, and heat flux, across the same
sample, we get

	Rt = L * T/(A * Hi * L) = T/Hi;

Which reiterate eq.1.


# GAIN AND TIME CONSTANTS

In the form of a type 0 linear system:

	Rt / (s * (Rt * C) + 1) = Kcc / (s * tau + 1)

Where: "Kcc" is a constant that defines the output variation
according to the input variation, Rt;
"tau" is another constant, which defines the time that the system
needs to set: Rt * C.

This values were determined by observation: tau is the time
needed for the system to reach 63.21 % of it's final value, and Kcc as the
ratio between the system entry of energy by the system loss of energy when
the system sets.

The practical system entry is actually voltage, a conversion
block is added to the system:

	P = V^2/R;

Which is given in joules. One Joule is Kph = 2.39 * 10^-4 Kcal, and so:

	Hi = Kph * V^2/R;

```

FIGURE 3 : Transfer function, taking voltage as entry

			+-------+		+-------+		+-------+
			|	|		|	|		|	|
	 -> V(s) ->	| V^2/R	|  -> P(s) ->	|  Kph	|  -> Hi(s) ->	| G(s)	|  -> T(s)
			|	|		|	|		|	|
			+-------+		+-------+		+-------+

The system varies in tension, which will cause a power variation,
thus, changing the system temperature;

```

AS Kcc is, actually, the thermal resistance of the system,

kcc = Rt = T/Hi; (eq.5)

tau = t, if g(t) = g(t->infinity) * 0.63; (eq.6)


# MODEL VALIDATION

The model validation consists in affering wheter your theorical
model is a valid aproximation for a practical system. Thus,
plotting both theorical and practical data in overlay is a
efficient way.


# NOISE IN MEASURE METHOD
As neither the measures, and the method used for measuring are
ideals, the function aquired is full of "spikes", which is
undesired. One method of smooth the function noise is to exchange
each time sample by it's average between n samples ahead, as
follows:

average-function(t) = sum(f(t))(from t to t+n)/n, where n is the
number of dots used in the average.

Chart 5 shows the comparsion between original (red) function and the
aproximated (green) function

![Chart 5](5.png)

Note: unless specified otherwise, the aproximated function will be
used.


# ELECTRICAL EQUIVALENT MODEL


# DATA ANALISYS AND CHOOSE OF MODEL
The chart 1.png shows the system response for the real system, in
red, the theorical response, using the practical constans, in
green, and the theorical response, using theorical constants, in
blue. Fo better view, the same functions are plot, sepratedly, in
charts 2 (experiment only), 3 (theorical, with practical constants),
and 4 (theorical, with theorical constants).
![Chart 1](1.png)
![Chart 2](2.png)
![Chart 3](3.png)
![Chart 4](4.png)

The most close curve to the practical system is not obvious,
at first glance, and for this ocasion, we'll calculate the
difference for each point in the system:

delta(t) = |exp_data(t) - g1_data(t)|

The results may be seen in chart 6

![Chart 6](6.png)

And, yet, the result is not clear to our eyes. We have one more
resouce, which is to calculate the total error accumulated in time: Integrating the error for every simulated point

integral(t) = delta(t), for t = 0

integral(t) = integral(t-1) + delta(t); t > 1

The results may be seen in the chart 7.

![Chart 7](7.png)
In chart 6 we see that, for aprox. 1800s, G2 has a bigger
difference from the experiment, almost 4 degrees, and consequently it's integral (chart 7)
raises faster; Though, G2 difference decreses arround 2700s,
getting as small as 0, having a second peak at 4200, with 1 degree
of difference. The integral as well turns to grow back
,but in smaller tax.

G1 stays closer to experiment until 1300s, in aprox. 1 degree, but separate quickly
until 3000s, and turns to bounce arround 3 degrees, setting majorly
arround this point.

In chart 7 we see that the G2 function difference grows
faster for t < 1600, the final integral value is bigger for G1, but
for the major part of the time G2 has a lower error growth tax than G1.
After analysing the charts, the ideal control system is G2, as the needed energy for correction
will be lower for the most time, in which the error sum stops
growing (after 2500s), and has the smaller error for every time
after 1800s.

 ACTUATOR EFFECTS IN CONTROL LOOP
There are several ways to control the system activity, whether
using MOSFETS, IGBT, direct sources, and other components. The IGBT
is a low voltage controled switch, providing fast switch when
polarized in the specified voltage.

As being a switch, it is possible to use as a PWM (Pulse Width
Modulation) source, when aplied a AC, resulting in a direct
conversion between the high voltage sourced from the original
voltage aplied in the heater (127V), and a reliable low voltage for
control: as low as 5V: the average voltage this system generates is
given as it follows:

```

FIGURE N1

	   V
	   ^
	VCC|   +---+   +---+
	   |   |   |   |   |
	   |   |   |   |   |
	   |   |   |   |   |
	   |___|   |___|   |
	   +---.---.--------> t(s)
	   0   TX  T

The PWM is a switchng between VCC and 0, in a given duty cycle
(ratio of vcc timing by 0v timing)

```

Toff = Tx
Ton = T-Tx

D = Ton/T;	

Defining integral as integral(V(t), To, Tf) = intregral of V(t), in
the interval from To (lower limit) to Tf (upper limit):

Vavg = 1/T  *  integral(V(t), 0, T)
= 1/T  *  (integral(Vcc, 0, Ton) + integral(0, Ton, T))
= Vcc  *  Ton/T
= Vcc  *  D; (eq. N2)

As the original model varies from 0 to 37V, the maximum avegare
voltage is defined. The maximum PWM output voltage is defined in
function of the duty cyle, and resultin into a saw-form:

D = Vpwmin/Vtripico


```

 	FIGURE N1+1

The relation between PWM cycles, saw-tooth function, and control
voltage

```

 ACTIVATION METHOD


# SOLID STATE RELAY

The selected method of handling the current through the circuit was
using a Solid State Relay, which works based in a PWM signal,
permiting the flow of current in it's output, is this case, the
power plug, with 127V as output. The practical voltage the heater
sees will be:

Vef = Vp * sqrt(Ton/(2 * T))

The relation between PWM cycles and the RMS voltage is not linear,
though> it's squared. The relation changes, then, to:

Vef^2 = Vp^2  *  Ton/(2 * T)

In practical terms, the grid voltage is 127Vrms, at 60Hz of
frequency, resulting in 180V DC. As the relay does not support
60Hz, the value will have only 6Hz, a peak of 10V, resulting in:

f = 6, thus T = 1/6

Vef = 180 * sqrt(1/6/(2 * (1/6))) = 127.28 Vrms

The total gain in this device is:

Kr = vout/vin = 127.28^2/10 = 1620.02


```

FIGURE 

```




















# HEATER SUPERFICIAL AREA
The heater consists in a resistance which circles a plastic
structure. It have small loops, enhancincg contact area (similar to
shower resistances). The first "loop" refers to those small loops.
The loop's loop refers to the circle made around plastic structure

Values sufixed by  *  are measured by the use of a caliper (+-0.5mm
of precision)

 	Radius (r) * 

Wire circunference (wc): wc = 2 * pi * r;

Loop Radius (lr) * 

Loop circunference (lc):

	lc = 2 * pi * lr;

Area of one loop (al):

	al = wc * lc;

Distance betwenn two loops (dl) * 

Radius of one loop's loop (lld) * 

Loop's loop circunference (llc):

	llc = 2 * pi * llr;

Area of one loop's loop (alll):

	alll = llc * al;

Number of loop's loop (llln) * 

Remaining not looped wire (nlw) * 

Total area:

	a = alll * lln + nlw;

# AIR CONTACT AREA
 	Cup Radius(cr):

Air contact area

	aca = pi * cr^2;

# STEEL CONTACT AREA

given by the volume of water divided by area of cup's base, which
gives the height:

	h = wm/aca;

And we must mutiply by the cup circunference, plus the area of the
bottom:

	sca = 2 * pi * cr * h + aca;

 CONTROL MODELING

# REFERENCES

[] citations/ogata.bibtex

[2 - Manual](http://www.icel-manaus.com.br/download/MD-6150%20Manual.pdf), last access: 04-28-2022 [MM-DD-YYYY]

[3] https://carlroth.com/medias/SDB-1980-PT-PT.pdf?context=bWFzdGVyfHNlY3VyaXR5RGF0YXNoZWV0c3wzMzg1MzZ8YXBwbGljYXRpb24vcGRmfHNlY3VyaXR5RGF0YXNoZWV0cy9oMzgvaGJjLzg5NzExMjAxNDg1MTAucGRmfDM3NGQzNDdhNzZkNjUwY2I3OThjODExNDZmZTJkODIwMTNiNWNlMjIyNTkxZTNkYzBlMmY5MDY4Mjc5NjA4ZDc

# THE MEASURING
For the measuring of water heating process, the following was used:
	 *  MD-6510 multimeter [1], Heater(770W);
