1;

function chart1(exp_data, g1_data, g2_data, n)
	global USEAVERAGE;

	figure;
	plot(1:n, exp_data, "r;Experimental;", 1:n+1, g1_data, "g;Teórico 1;", 1:n+1, g2_data, "b;Teórico 2;");
	if USEAVERAGE
		title('GRAF. 1: Comparação de modelos: Prático(aprox.) e teórico');
	else
		title('GRAF. 1: Models comparsion: Practical and theorical');
	endif
	xlabel ("tempo (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	legend("location", "southeast");
	print -dpng "1.png"
endfunction

function chart2(exp_data, n)
	global USEAVERAGE;

	figure;
	plot(1:n, exp_data, "r;Experimental;");
	if USEAVERAGE
		title('GRAF. 2: Modelo Experimental (aprox.)');
	else
		title('CHART 2: Modelo Experimental');
	endif
	xlabel ("tempo (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "2.png"
endfunction

function chart3(g1_data, n)
	figure;
	plot(1:n, g1_data(1:n), "g;Teórico 1;");
	title('GRAF. 3: Modelo Teórico 1');
	xlabel ("tempo (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "3.png"
endfunction

function chart4(g2_data, n)
	figure;
	plot(1:n, g2_data(1:n), "b;Teórico 2;");
	title('GRAF 4: Modelo Teórico 2');
	xlabel ("tempo (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "4.png"
endfunction

function chart5(exp_data, avgexp_data, n)
	interval=n/16;
	figure;
	plot(1:interval, exp_data(1:interval), "r;Original;", 1:interval, avgexp_data(1:interval), "m;Aproximado;");
	title('GRAF. 5: Comparação entre modelo experimental original, e aproximado');
	xlabel ("tempo (s)");
	ylabel ("temp (ºC)");
	legend("location", "southeast");
	print -dpng "5.png"
endfunction

function chart6(delta1, delta2, n)
	global USEAVERAGE;

	figure;
	plot(1:n, delta1, "g;|G1-exp|;", 1:n, delta2, "b;|G2-exp|;");
	if USEAVERAGE
		title('GRAF. 6: Diferença de sistemas experimental (aprox.) para G1 e G2');
	else
		title('GRAF. 6: Diferença de sistemas experimental para G1 e G2');
	endif
	xlabel ("tempo (s)");
	ylabel ("d(dT) (ºC)");
	legend("location", "southeast");
	print -dpng "6.png"
endfunction

function chart7(integ1, integ2, n)	
	global USEAVERAGE;

	figure;
	plot(1:n, integ1, "g;integral (|G1-exp|);", 1:n, integ2, "b;integral (|G2-exp|);");
	if USEAVERAGE
		title('GRAF. 7: Integral da diferença entra sistema experimental (aprox.) para G1 e G2');
	else
		title('GRAF. 7: Integral da diferença entra sistema experimental para G1 e G2');
	endif
	xlabel ("tempo (s)");
	ylabel ("d(dT) (ºC)");
	legend("location", "southeast");
	print -dpng "7.png"
endfunction

